// vite.config.ts
import { defineConfig } from 'vitest/config';
import postcssLit from 'rollup-plugin-postcss-lit';

export default defineConfig({
  test: {
    browser: {
      enabled: true,
      name: 'chrome',
    },
  },
  plugins: [postcssLit()],
});
