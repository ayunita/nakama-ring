// vite.config.ts
import { defineConfig } from 'vitest/config';
import postcssLit from 'rollup-plugin-postcss-lit';

export default defineConfig({
  test: {
    browser: {
      enabled: true,
      name: 'chrome',
    },
  },
  build: {
    lib: {
      entry: 'src/index.ts',
      formats: ['es'],
      name: 'NakamaRing',
    },
    outDir: 'lib',
    rollupOptions: {
      external: /^lit/,
    },
  },
  plugins: [postcssLit()],
});
