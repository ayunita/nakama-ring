<div align='center'>
    <h1><b>Nakama Ring</b></h1>
    <img src='https://i.postimg.cc/MpY6wgd6/nakamaring.png' width='300' />
</div>

---

## ⭐ **ABOUT**

An example of webring built using Lit.

Feel free to use and change it!
<br />

---

## 🛠️ **INSTALLATION**

### local development:

1. clone the repo

```
git clone https://gitlab.com/ayunita/nakama-ring.git
```

2. cd into cloned repo

```
cd nakama-ring
```

3. install dependencies

```
npm install
```

4. run the app

```
npm run dev
```

5. run unit test

```
npm run test
```

6. build the library

```
npm run build
```

### example of local installation:

1. install dependency

```
npm i @ayunita/nakama-ring
```

2. import the element

```
import { LitElement, html } from "lit";
import "@ayunita/nakama-ring"

...

render() {
    return html`
        <nakama-ring
          font-variant="comic"
          size="medium"
          site-url="https://www.amazon.com/"
          member-src="https://api.npoint.io/b7e62bd1a5d05369e29d">
        </nakama-ring>
    `;
}
```

<br />

## 🔎 **SHOWCASE**

[Demo page](https://nakama-ring-ayunita-602f6559a13b118367d4b20ec1487a0abda2e2dc923.gitlab.io/)
<br />

---

## 💻 **TECHNOLOGIES**
![Lit](https://img.shields.io/badge/lit-324FFF?style=for-the-badge&logo=lit&logoColor=white)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![Vite](https://img.shields.io/badge/vite-%23646CFF.svg?style=for-the-badge&logo=vite&logoColor=white)

<br />