import { beforeEach, describe, expect, it } from 'vitest';

import './nakama-ring';

describe('<nakama-ring> ', async () => {
  function getElement(): HTMLElement | null | undefined {
    return document.body.querySelector('nakama-ring');
  }

  describe('when provided no parameters', async () => {
    beforeEach(() => {
      document.body.innerHTML = '<nakama-ring></nakama-ring>';
    });

    it('should render element', () => {
      expect(
        getElement()
          ?.shadowRoot?.querySelector('.nakama_ring')
          ?.className.trim(),
      ).toEqual('nakama_ring');
    });
  });

  describe('when set list-disabled', () => {
    beforeEach(() => {
      document.body.innerHTML = '<nakama-ring list-disabled></nakama-ring>';
    });

    it('should remove list button', () => {
      expect(
        getElement()
          ?.shadowRoot?.querySelector('.nakama_ring__nav')
          ?.textContent?.replace(/\s+/g, ''),
      ).toEqual('PreviousRandomNext');
    });
  });

  describe('when set random-disabled', () => {
    beforeEach(() => {
      document.body.innerHTML = '<nakama-ring random-disabled></nakama-ring>';
    });

    it('should remove random button', () => {
      expect(
        getElement()
          ?.shadowRoot?.querySelector('.nakama_ring__nav')
          ?.textContent?.replace(/\s+/g, ''),
      ).toEqual('PreviousNextList');
    });
  });
});
