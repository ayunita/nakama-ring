import { html, unsafeCSS } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { classMap } from 'lit/directives/class-map.js';

import BaseElement from '../base-element';
import styles from './nakama-ring.css?inline';
import banner from '../../assets/nakama_hand_transparent.png';

interface Nakama {
  webmaster?: string;
  title?: string;
  desc?: string;
  url?: string;
  image?: string;
}

enum Status {
  Fetching = 'Fetching nakama-ring members',
  Required = 'member-src is missing',
  Failed = 'Failed to get the member list',
  Error = 'Error in fetching data',
  Success = '',
}

enum FontVariant {
  Times = 'times',
  Comic = 'comic',
  Inherit = 'inherit',
}

enum BoxSize {
  Small = 'small',
  Medium = 'medium',
  Large = 'large',
  Full = 'full',
  Auto = 'auto',
}

/**
 * @summary Custom webring.
 *
 * @slot content - The webring's content.
 *
 * @csspart content - The wrapper for webring's content.
 * @csspart nav - The wrapper for webring's navigation.
 * @csspart list - The wrapper for displaying webring's member list.
 */
@customElement('nakama-ring')
export default class NakamaRing extends BaseElement {
  static styles = unsafeCSS(styles);

  /** Member's website URL */
  @property({ type: String, attribute: 'site-url' }) siteUrl = '';

  /** The URL to the JSON file containig members' information */
  @property({ type: String, attribute: 'member-src' }) memberSrc = '';

  /** If set, remove list button */
  @property({ type: Boolean, attribute: 'list-disabled', reflect: true })
  listDisabled = false;

  /** If set, remove random button */
  @property({ type: Boolean, attribute: 'random-disabled', reflect: true })
  randomDisabled = false;

  /** The webring's font variant. */
  @property({ type: String, attribute: 'font-variant' })
  fontVariant: FontVariant = FontVariant.Inherit;

  /** The webring's width size. */
  @property({ type: String }) size: BoxSize = BoxSize.Auto;

  @state() status: Status = Status.Fetching;
  @state() current = 0;
  @state() next = 0;
  @state() prev = 0;
  @state() random = 0;
  @state() open = false;
  @state() list: Array<Nakama> = [];

  async firstUpdated() {
    this._getWebList();
  }

  async _getWebList() {
    if (!this.memberSrc) {
      this.status = Status.Required;
      return;
    }

    // Gets member list from json file
    const response = await fetch(this.memberSrc);
    if (!response.ok) {
      this.status = Status.Failed;
      return;
    }

    // Sets the element's nav
    try {
      const members = await response.json();
      this.list = members;
      const currentIndex = this.list.findIndex(web => web.url === this.siteUrl);
      this.current = currentIndex;
      this.next = currentIndex + 1;
      this.prev = currentIndex > 0 ? currentIndex - 1 : 0;
      this.random = this._generateRandomNumber(this.list.length, this.current);
      this.status = Status.Success;
    } catch (error) {
      this.status = Status.Error;
    }
  }

  _generateRandomNumber(max: number, ignore: number) {
    let randomNumber;
    do {
      randomNumber = Math.floor(Math.random() * max);
    } while (randomNumber === ignore);
    return randomNumber;
  }

  _toggleList() {
    this.open = !this.open;
  }

  render() {
    return html`
      ${this.status}
      <div
        class=${classMap({
          nakama_ring: true,
          'nakama_ring--times': this.fontVariant === FontVariant.Times,
          'nakama_ring--comic': this.fontVariant === FontVariant.Comic,
          'nakama_ring--small': this.size === BoxSize.Small,
          'nakama_ring--medium': this.size === BoxSize.Medium,
          'nakama_ring--large': this.size === BoxSize.Large,
          'nakama_ring--full': this.size === BoxSize.Full,
        })}
      >
        <!-- Content -->
        <slot name="content" part="content">
          <div class="nakama_ring__default">
            <img src=${banner} alt="banner" class="nakama_ring__image" />
            <div>
              <p>
                This
                <a href="${this.list[this.current]?.url}"
                  >${this.list[this.current]?.title}</a
                >
                site is owned by ${this.list[this.current]?.webmaster}.
              </p>
              <p>
                Want to join this ring?
                <a href="${this.memberSrc}" rel="noreferrer noopener"
                  >Click here!</a
                >
              </p>
            </div>
          </div>
        </slot>
        <!-- #Content -->
        <div class="nakama_ring__divider"></div>
        <!-- Nav -->
        <div class="nakama_ring__nav" part="nav">
          <a href="${this.list[this.prev]?.url}" rel="noreferrer noopener"
            >Previous</a
          >
          ${!this.randomDisabled
            ? html`<a
                href="${this.list[this.random]?.url}"
                rel="noreferrer noopener"
                >Random</a
              >`
            : ''}
          <a href="${this.list[this.next]?.url}" rel="noreferrer noopener"
            >Next</a
          >
          ${!this.listDisabled
            ? html`<a
                class="nakama_ring__button_link"
                aria-controls="nakama-ring-list"
                aria-expanded="${this.open ? true : false}"
                role="button"
                tabindex="0"
                @click="${this._toggleList}"
                >List</a
              >`
            : ''}
        </div>
        <!-- #Nav -->
        <div
          id="nakama-ring-list"
          aria-hidden="${this.open ? false : true}"
          class=${classMap({
            nakama_ring__list: true,
            'nakama_ring__list--show': this.open,
          })}
          part="list"
        >
          ${this.list.map(
            web =>
              html`<div class="nakama_ring__list__item">
                <img src="${web.image}" alt="logo" />
                <div>
                  <span>
                    ${web.webmaster}
                    <a href="${web.url}" rel="noreferrer noopener"
                      >${web.url}</a
                    >
                  </span>
                  <span>${web.desc}</span>
                </div>
              </div>`,
          )}
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'nakama-ring': NakamaRing;
  }
}
